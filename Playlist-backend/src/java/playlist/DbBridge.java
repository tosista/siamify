/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package playlist;

import java.sql.DriverManager;
import com.mysql.cj.jdbc.Driver;
import java.sql.SQLException;
import java.util.Properties;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import playlist.model.Playlist;
import playlist.model.Song;

/**
 *
 * @author Siam1838
 */
public class DbBridge {

    private Properties properties;
    private final String uri = "jdbc:mysql://127.0.0.1:3306";

    // a quick debuggin main method
    public static void main(String[] args) {
        DbBridge bridge = new DbBridge();
        bridge.getSongs();
    }

    // constructor with properties creation
    public DbBridge() {
        try {
            DriverManager.registerDriver(new Driver());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        this.properties = new Properties();
        this.properties.put("useSSL", "false");
        this.properties.put("allowPublicKeyRetrieval", "true");
        this.properties.put("user", "root");
        this.properties.put("password", "VeryStr0ngP@ssw0rd");
    }

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(this.uri, this.properties);
    }

    public List<Song> getSongs() {
        String query = "SELECT id_song, title, artist, duration FROM playlist.songs;";
        try {
            // creaiamo per direttissima il ResultSet invocando il getConncection e creando inline il prepared statement
            ResultSet rs = this.getConnection()
                    .prepareStatement(query)
                    .executeQuery();
            List<Song> songs = new ArrayList<>();
            while (rs.next()) {
                Song song = new Song(
                        // argomenti numerici ai get indicano posizione nella riga del ResulSet
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4)
                );
                songs.add(song);
            }
            return songs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public List<Playlist> getPlaylists() {
        // TODO a first version of this method that retrieves the Playlist name and duration
        String query = "SELECT id_playlist, name, duration FROM playlist.playlist;";
        try {
            ResultSet rs = this.getConnection()
                    .prepareStatement(query)
                    .executeQuery();
            List<Playlist> playlists = new ArrayList<>();
            while(rs.next()){
                Playlist tmpPlaylist = new Playlist(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        null
                );
                playlists.add(tmpPlaylist);
            }
            return playlists;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    public void updatePlaylists() {
        
    }

}
