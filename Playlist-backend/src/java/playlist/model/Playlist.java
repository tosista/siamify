/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package playlist.model;

import java.util.List;

/**
 *
 * @author Siam1838
 */
public class Playlist {
    
    private int id;
    private String name;
    private int duration;
    private List<Song> songsList;

    public Playlist() {
    }

    public Playlist(int id, String name, int duration, List<Song> songsList) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.songsList = songsList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<Song> getSongsList() {
        return songsList;
    }

    public void setSongsList(List<Song> songsList) {
        this.songsList = songsList;
    }
    
}
