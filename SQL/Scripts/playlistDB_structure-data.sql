CREATE DATABASE playlist;
USE playlist;

CREATE TABLE songs (
	id_song int primary key auto_increment,
	title varchar(255),
	artist varchar(255),
	duration int
);

CREATE TABLE playlist (
	id_playlist int primary key auto_increment,
    name varchar(255),
    durata int null
);

CREATE TABLE playlist_song (
	id_song int not null,
    id_playlist int not null,
    position int null,
    constraint PK_playlist_song primary key (id_song, id_playlist asc),
    constraint FK_playlist_song_song foreign key (id_song) references songs(id_song),
    constraint FK_playlist_song_playlist foreign key (id_playlist) references playlist(id_playlist)
);

INSERT INTO songs (title, artist, duration)
VALUES 
("Casa mia", "Ghali", 210),
("Fino a qui", "Alessandra Amoroso", 232),
("Tutto Qui", "Gazzelle", 211),
("Ma non tutta la vita", "Ricchi e Poveri", 190),
("Onda Alta","Dargen D'Amico", 214),
("La noia", "Angelina Mango", 189),
("Il cielo non ci vuole", "Fred De Palma", 172),
("Mariposa","Fiorella Mannoia", 177),
("Pazza", "Loredana Berte", 168),
("Due altalene","Mr. Rain", 197),
("I p'' me, tu p'' te", "Geolier", 187),
("Ricominciamo tutto", "Negramaro", 232),
("Click Boom!", "Rose Villain", 226),
("Tuta Gold", "Mahmood", 178),
("Ti muovi", "Diodato", 187),
("Sinceramente", "Annalisa", 215),
("Capolavoro", "Il Volo", 198),
("Apnea","Emma", 180),
("Pazzo di te", "Francesco Renga e Nek", 177),
("Autodistruttivo", "La Sad", 189),
("Tu no","Irama", 215),
("La rabbia non ti basta","BigMama", 189),
("Un ragazzo una ragazza", "The Kolors", 217),
("Finiscimi","Sangiovanni", 191),
("Fragili", "Il Tre", 189),
("Vai!","Alfa", 157),
("Spettacolare", "Maninni", 173),
("Diamanti grezzi","Clara", 192),
("Governo punk","bnkr44", 205),
("L''amore in bocca","Santi Francesi", 172);

UPDATE songs SET title=title, artist='Loredana Bertè', duration=duration WHERE id_song=9;
SELECT * FROM songs;

INSERT INTO playlist (name, duration) VALUES ("Cavalli", 0);
INSERT INTO playlist (name, duration) VALUES ("Intelligenze Artificiali", 0);

SELECT * FROM playlist;
UPDATE playlist SET name='Ma c''erano queste?', duration=duration WHERE id_playlist=2;

SELECT * FROM songs s INNER JOIN playlist_song ps ON s.id_song = ps.id_song;