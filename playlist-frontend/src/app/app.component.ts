import { Component } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { AppModule } from './app/app.module';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, AppModule, RouterModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'playlist';
}
