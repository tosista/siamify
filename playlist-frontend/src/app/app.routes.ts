import { Routes } from '@angular/router';
import { ViewCatalogueComponent } from './view-catalogue/view-catalogue.component';

export const routes: Routes = [
  { path: 'catalog', component: ViewCatalogueComponent },
];
