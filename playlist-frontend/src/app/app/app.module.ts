import { NgModule, importProvidersFrom } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewCatalogueComponent } from '../view-catalogue/view-catalogue.component';
import { SongsComponent } from '../songs/songs.component';
import { PlaylistComponent } from '../playlist/playlist.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [ViewCatalogueComponent, SongsComponent, PlaylistComponent],
  imports: [CommonModule],
  exports: [ViewCatalogueComponent, PlaylistComponent],
  // remember to inject the HttpClient module this way!
  providers: [importProvidersFrom(HttpClientModule)],
})
export class AppModule {}
