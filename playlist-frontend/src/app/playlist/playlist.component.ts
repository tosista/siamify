import { HttpClient } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { Playlist } from '../model/playlist.model';
import { Song } from '../model/song.model';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrl: './playlist.component.css',
})
export class PlaylistComponent {
  // Variable that stores the currently selected playlist
  protected selectedPlaylist!: Playlist;
  // Method that sets the selectedPlaylist property
  protected select(selected: Playlist) {
    this.selectedPlaylist = selected;
  }

  // Input that binds the value of the currently selected song
  // value starts from Song.component, then outputs to ViewCatalogue.component
  @Input() selectedSong!: Song;

  // HTTP get request to fetch the playlists
  private uri: string = 'http://localhost:8080/playlist/playlists';

  protected playlists: Playlist[] = [];

  constructor(private http: HttpClient) {
    this.http.get<Playlist[]>(this.uri).subscribe((res) => {
      this.playlists = res;
    });
  }

  // Method that adds selected song to selected playlist
  protected add() {
    if (!this.selectedPlaylist.songsList) {
      this.selectedPlaylist.songsList = [];
    }
    // add the currently selected song to the playlist
    this.selectedPlaylist.songsList.push(this.selectedSong);

    // set updated duration for each playlist
    this.playlists.forEach((playlist) => {
      let duration = 0;
      playlist.songsList?.forEach((song) => {
        duration += song.duration;
      });
      playlist.duration = duration;
    });

    // HTTP post request to backend
    // first we make the json, than we do the post request
    const json = JSON.stringify(this.playlists);
    this.http.post(this.uri, json).subscribe(() => {});
  }
}
