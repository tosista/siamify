import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Output } from '@angular/core';
import { Song } from '../model/song.model';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrl: './songs.component.css',
})
export class SongsComponent {
  // Variable to save the currently selected song
  // the '!' tells angular that property could be undefined
  protected selected!: Song;

  // HTTP getting the list of songs

  protected songs: Song[] = [];
  private uri: string = 'http://localhost:8080/playlist/songs';
  constructor(private http: HttpClient) {
    // with the diamond operator we say to the get that we expect an array of Song, formatted as json
    this.http.get<Song[]>(this.uri).subscribe((res) => {
      this.songs = res;
    });
  }

  //OUTPUT
  // this will send to other components the currently selected song
  @Output() selectedSong = new EventEmitter<Song>();

  // METHODS

  // function
  protected select(selected: Song) {
    this.selected = selected;
    // emit the new selected song
    this.selectedSong.emit(this.selected);
  }
}
