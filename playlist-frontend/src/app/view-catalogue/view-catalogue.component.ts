import { Component } from '@angular/core';
import { Song } from '../model/song.model';

@Component({
  selector: 'app-view-catalogue',
  templateUrl: './view-catalogue.component.html',
  styleUrl: './view-catalogue.component.css',
})
export class ViewCatalogueComponent {
  protected selectedSong!: Song;
}
