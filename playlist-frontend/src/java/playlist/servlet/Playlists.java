/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package playlist.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import playlist.DbBridge;
import playlist.model.Playlist;

/**
 *
 * @author Siam1838
 */
@WebServlet(name = "Playlists", urlPatterns = {"/playlists"})
public class Playlists extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // preparing the RESPONSE
        PrintWriter out = response.getWriter();
        // allow cross origin access
        response.setHeader("Access-Control-Allow-Origin", "*");
        
        // create the bridge object to the DB
        DbBridge bridge = new DbBridge();
        List<Playlist> playlists = bridge.getPlaylists();
        
        // create the json builder project
        Jsonb jb = JsonbBuilder.create();
        
        // parse the data from Playlist list to json
        String json = jb.toJson(playlists);
        
        // write the json to the response
        out.println(json);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Create the reader from the request
        BufferedReader in = request.getReader();
        response.setHeader("Access-Control-Allow-Origin", "*");
        // Read the json sent in the request
        String json = in.readLine();
        // Transform the json back into a List of Playlist
        Jsonb jb = JsonbBuilder.create();
        // the fromJson needs the kind of object that we are receiving (and its class)
        List<Playlist> playlists = jb.fromJson(json, List.class);
        // sout to check if data is received correctly
        System.out.println(playlists);
        
        //TODO Save the List via the addPlaylist method of the DbBridge
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
